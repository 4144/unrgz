#! /usr/bin/env python2

import os
import re
import sys
import shutil
import subprocess


def showHelp():
    print("Usage: clientunpacker.py rgzfile")


def cleanup():
    if os.path.exists("tmpdir"):
        shutil.rmtree("tmpdir")
    os.makedirs("tmpdir")
    if os.path.exists("unpacked") is False:
        os.makedirs("unpacked")


def extractFile():
    if fileName[-4:] != ".rgz":
        print("Error: file is not rgz")
        exit(1)
    os.chdir("tmpdir")
    subprocess.call(["../unrgz.py", "../" + fileName])
    os.chdir("..")


def searchFiles():
    exeRe = re.compile("[.]exe")
    files = os.listdir("tmpdir")
    exeFiles = []
    cnt = 0
    for file1 in files:
        m = exeRe.search(file1)
        if m is not None:
            exeFiles.append(file1)
        cnt = cnt + 1
    return (exeFiles, cnt)


def getDateStr():
    dateRe = re.compile("^(?P<v1>[\d][\d][\d][\d])(-|_)(?P<v2>([\d][\d]|[\d]))(-|_)(?P<v3>([\d][\d]|[\d]))(?P<v4>((_[\d])|([\d])|))(?P<other>.+)")
    m = dateRe.match(fileName)
    if m is None:
        print("Error: rgz file without date.")
        exit(1)
    v1 = m.group("v1")
    v2 = m.group("v2")
    v2 = ("0" * (2 - len(v2))) + v2
    v3 = m.group("v3")
    v3 = ("0" * (2 - len(v3))) + v3
    v4 = m.group("v4")
    other = m.group("other")
    if other.find("data") == 0:
        other = other[4:]
    if other[0] == "_":
        other = other[1:]
    if other.find("gm_data_") == 0:
        other = "gm_" + other[8:]
    return ("{0}-{1}-{2}{3}".format(v1, v2, v3, v4), other)


def selectNewName(dstName):
    numExeRe = re.compile("_([\d]+)$")
    name = dstName[:-4]
    m = numExeRe.search(name)
    if m is not None:
        idx = name.rfind("_")
        if idx >= 0:
            name = name[:idx]
    num = 2
    while os.path.exists("{0}_{1}.exe".format(name, num)):
        num = num + 1
    return "{0}_{1}.exe".format(name, num)


def copyFiles(files):
    if len(files) == 0:
        print("Warning: no exe files present in {0}".format(fileName))
        exit(0)
    if flag == "hidden" and filesCount == 1 and files[0][-4:] == ".exe":
        print("Warning: skip non hidden clients {0}".format(fileName))
        exit(0)
    isError = False
    for file1 in files:
        ret = copyFile(file1)
        if ret is True:
            isError = True
    if isError is True:
        exit(1)


def copyFile(file1):
    testName = file1.lower()
    if testName in ("ragexe.exe", "ragexe_up.exe", "ragexe_2.exe", "ragexe_3.exe"):
        dstName = "Ragexe.exe"
    elif testName in ("ragexere.exe", "ragexere_2.exe", "ragexere_3.exe"):
        dstName = "RagexeRE.exe"
    elif testName == "sakexe.exe":
        dstName = "Sakexe.exe"
    elif testName == "clragexe.exe":
        dstName = "clRagexe.exe"
    elif testName == "cmragexe.exe":
        dstName = "cmRagexe.exe"
    elif testName.find(".exe.manifest") >= 0:
        print("Skip other exe files: {0}".format(file1))
        return False
    elif testName in ("ragnarok.exe", "setup_classic.exe", "classicro.exe", \
                      "sakray.exe", "patchup.exe", "setup.exe", "patchupzero.exe", \
                      "patchup_ad.exe", "asplnchr.exe", "patchup_re.exe", \
                      "savepath_rag.exe", "savepath_sak.exe", "init.exe", \
                      "ragnarokreplay.exe"):
        print("Skip other exe files: {0}".format(file1))
        return False
    else:
        print("Error: unknown exe file found: {0}".format(file1))
        return True

    idx = fileName2.find("exe")
    if idx >= 0 and idx != fileName2.find("_exe") + 1:
        dstName = dateStr + fileName2[:-4] + ".exe"
        dstName = dstName.replace("ragexe", "Ragexe")
        dstName = dstName.replace("sakexe", "Sakexe")
        dstName = dstName.replace("clragexe", "clRagexe")
        dstName = dstName.replace("cmragexe", "cmRagexe")
    else:
        dstName = dateStr + dstName
    exeName = "unpacked/" + dstName
    if os.path.exists(exeName) is True:
        dstName = selectNewName(dstName)
        exeName = "unpacked/" + dstName
    shutil.copyfile("tmpdir/" + file1, exeName)
    print("Unpacked: {0}".format(dstName))
    return False


def getFileName():
    if len(sys.argv) == 3:
        flag = sys.argv[1]
        fileName = sys.argv[2]
    else:
        flag = ""
        fileName = sys.argv[1]
    return flag, fileName


if len(sys.argv) < 2:
    showHelp()
    exit(1)

flag, fileName = getFileName()
cleanup()
extractFile()
dateStr, fileName2 = getDateStr()
files, filesCount = searchFiles()
copyFiles(files)
